* BMarXiv (Busy Man's arXiv)
- BMarXiv scans new (i.e., since the last time checked) submissions from arXiv, ranks submissions based on keyword matches, and produces an HTML page as an output. 
- The keywords are looked for (with regex capabilities) in the title, abstract, but also the author list, so it is possible to look for people too. The score is calculated for each specific entry but additional (and optional) scoring is performed using the first author recent submissions and/or the other authors' recent submissions. 
- It is possible to include/exclude any arXiv categories (within astro-ph or not). New astronomical conferences (from CADC by default) and new codes (from ASCL.net) are also checked and can also be scanned for keywords. 
- A local bibliography file can be scanned to find frequent words/groups of words that could become scanned keywords.
- The script will look for new entries from the last time it was run. Note however that results are not ingested continuously but in batches each night shortly after new submissions are announced (10pm EST Sunday through Thursday). Manually change the lastchecked fields (see below) if you wish to scan starting from another date. 
- Results are provided in an HTML file. The sections in the output are as follows:
  - New entries with keyword match.
  - Entries with good author score but no keyword match (may be useful to add some keywords so that these papers could have been identified in the first section).
  - Revised entries with keyword match.
  - Entries with zero score. 

[[example/example_mygroup.png]]

[[example/example_mylisting.png]]
* Conferences
- The script can look up [[https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/en/meetings/][astrophysical conferences]] and show them based on their publication date. Right now it works with the astronomy listing from CADC but little tweaks would be needed to make it work with another URL. 
- Conferences are shown in a separate output file, but they are also merged in the main arXiv output and are scanned for keywords the same way. 

[[example/example_confs.png]]
* Codes
- The script can look up new codes from [[https://ascl.net/][ASCL]] and show them based on their publication date. 
- To use this feature, you will need to setup an ADS token (provide file location or the token directly in the token sub-field under the ads field (see below). 
- Codes are shown in a separate output file, but they are also merged in the main arXiv output and are scanned for keywords the same way. 

[[example/example_codes.png]]
* How to use
** Requirements & installation
- Needs Python 3. 
- Needs some packages:
#+BEGIN_SRC bash
pip install tqdm requests feedparser ruamel.yaml
#+END_SRC
- Also works with pyyaml but ruamel is used to keep comments in yaml file
- Install locally with
#+BEGIN_SRC bash
git clone https://gitlab.com/AstroVian/bmarxiv
#+END_SRC
- Launch the script within the installation directory (see below)
** General
- If you used to use the online BMarXiv tool, the main difference here is that the script needs to be run *locally*, with Python 3. 
- A YAML configuration script needs to be written (see below). 
- Run the program to scan new submissions. 
#+BEGIN_SRC bash
python bmarxiv.py
#+END_SRC
- The default browser should launch automatically to display the HTML output (see browser options below if not).
- HTML files are generated in the /results subdirectory.
- If you simply wish to review the output, run with -d to display the pages from the last check.
#+BEGIN_SRC bash
python bmarxiv.py -d
#+END_SRC
** Configuration script
- By default the script will look for the file config.yaml in the same directory. 
- You can create your own file following the example file in example/example.yaml. Note that if you choose the example file, *the last checked date will be so far behind that the script will look for the maximum allowed number of papers* (2000 by default; can be changed in the configuration file), so it is advised to change the date to something closer
*** Global parameters
- *categories*: arXiv's categories to include or exclude. Example:
#+BEGIN_SRC yaml
categories:
  excluded:
  - astro-ph.EP
  included:
  - astro-ph.GA
  - astro-ph.CO
  - astro-ph.HE
  - astro-ph.IM
  - astro-ph.SR
#+END_SRC
- *collapseauthors*: show only the number of authors before collapsing the rest into "et al.". Example: collapse after 15th author:
#+BEGIN_SRC yaml
collapseauthors: 15
#+END_SRC
- *eastcoast*: provide your time difference with EST. Example: EST - 6 hours:
#+BEGIN_SRC yaml
eastcoast: -6
#+END_SRC
- *max_results*: maximum number of results for the listing for checking recent submissions of the first author and of the other authors. 
#+BEGIN_SRC yaml
max_results:
  firstauthor: 10
  listing: 2000
  otherauthors: 10
#+END_SRC
- *scores*: whether to check the score of recent submissions of the first author and of the other authors. Example, refine scores using recent submissions from first author but don't consider recent submissions from other authors (it takes a while):
#+BEGIN_SRC yaml
scores:
  firstauthor: 1
  otherauthors: 0
#+END_SRC
- *queries*: define sets of keywords and override global parameters. Example:
#+BEGIN_SRC yaml
queries:
  mylisting:
    avoid:
    - globular.?cluster\w*
    - planet\w?
    - exoplanet\w*
    disabled: 0
    keywords:
    - the
    - bayesian
    - galax\w*
    - star.{1-20}rate\w*
    - star.?formation
    - absorption.?line\w*
    - absorption.?spectr\w+
    - emission.?lin\w+
    - emission.?spectr\w+
    - dwarf.?galax\w+
    - BCD\w+
    - blue.?compact.?dwarf\w+
    - '"HII region"'
    - '"PDR"'
    - '"SMC"'
    - '"LMC"'
#+END_SRC
**** *conferences*: 
- Optionally provide URL of listing. For now only the CADC website URL is accepted:
#+BEGIN_SRC yaml
conferences:
  url: https://www.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/meetings/rssFeed
#+END_SRC
**** *ads*
- Enter a token from ADS (https://ui.adsabs.harvard.edu/user/settings/token) either the token itself of the path to the file where the token lies
#+BEGIN_SRC yaml
ads:
  token: /local/home/myname/tokens/ads.token
#+END_SRC
or 
#+BEGIN_SRC yaml
ads:
  token: ViXjxCuqidHirZwfYXArYwUSPdKuVq
#+END_SRC
**** Astrophysical codes (ascl.net)
- If an ADS token is set (see above), codes from ASCL.net will be looked for automatically. 
*** Parameters for each individual query
- *disabled*: enable/disable specific query.
#+BEGIN_SRC yaml
#+END_SRC
- *avoid*/*keywords*: list of keywords (see below for syntax). 
#+BEGIN_SRC yaml
#+END_SRC
- you may also override *categories* and *scores* for each individual query.
*** Syntax for keyword match
**** General 
- Some general documentation on regex: https://www.w3schools.com/python/python_regex.asp
- By default, all keywords are searched assuming a non-letter/number character is on either side to avoid picking only part of a word (regex: \W+|^ and \W+|$).
- if you do wanna look for part of a word, use \w* (e.g., galax\w*)
**** Regex
- By default all matches are case-insensitive, use '"....."' for case-sensitive matches. 
- Some useful syntax: 
  - .:  matches any character
  - ?: 0 or 1 occurence
  - *: >=0 occurences
  - +: >0 occurences
  - ^,$: starts,ends with
  - []: set of characters
  - \w: alphanumerical characters
  - \W: non-alphanumerical characters
**** Examples
- For the galaxy name NGC1140, use NGC.?1140 (for ngc1140, NGC1140, ngc 1140, NGC 1140)
- For all words related to galaxies, use galax\w* (for galaxies, galaxy)
- For absorption-spectra, use absorption.?spectr\w+
- In general for a sequence of words use word1.?word2 to allow space, dashes or anything else between words, or simply word1 word2 to search only for the sequence with a space separator. 
- For the Large Magellanic Cloud, use '"LMC"'
*** Test configuration with a specific arXiv ID
#+BEGIN_SRC bash
python bmarxiv.py -i 2207.056757
#+END_SRC
- The default browser should launch automatically to display the HTML output (see browser options below if not).
- HTML files are generated in the /results subdirectory.
*** Use several configuration files
- You can run the script with different configuration files:
#+BEGIN_SRC bash
python bmarxiv.py -c another_file.yaml
#+END_SRC
** Browser options
- By default the code runs the default browser.
- It is possible to force the use of a specific browser in the configuration file, as shown in the following examples:
#+BEGIN_SRC yaml
browser:
  command: 'brave-browser %s'
#+END_SRC
#+BEGIN_SRC yaml
browser:
  command: 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
#+END_SRC
#+BEGIN_SRC yaml
browser:
  command: 'open -a /Applications/Google\ Chrome.app %s'
#+END_SRC
** Command-line options
- Most frequent options
|--------+-------------------------------------------------|
| Option |                                                 |
|--------+-------------------------------------------------|
| -q     | Check/display only specific query in yaml file  |
| -d     | Display last results (see also -q)              |
| -i     | Check specific arXiv ID  (see also -q)          |
| -b     | Scan bibliography file for keywords  (needs -q) |
|--------+-------------------------------------------------|

- Normally rarely used
|--------+----------------------------------------------------------|
| Option |                                                          |
|--------+----------------------------------------------------------|
| -c     | Use specific configuration file (other than config.yaml) |
| -l     | Check latest listing only (not until last time checked)  |
| -sin   | Use in script mode                                       |
|--------+----------------------------------------------------------|
* Bibliography file scan
- With the option -b it is possible to scan for frequent words (or groups of 2 or 3 words) in the title and abstract bibtex fields. 
- For speed, bmarxiv will select a random sample of 1000 entries.
- The automatically opened HTML page will then display a list of frequent words/groups of words, highlighting those that are not considered by the list of keywords defined in the yaml file query. 
- The HTML page will also display potential keywords defined in the yaml file query that have no match in this sample. These keywords may not be formatted correctly.
* Acknowledgements
Big thanks to arXiv for use of its open access interoperability.
* BibTeXendum
- Check this other tool! [[https://gitlab.com/AstroVian/bibtexendum/]]
- BibTeXendum can be used to automatically manage a bibliography file (bibtex entries). The script will scan preprints and update PDFs from the publisher, it cleans the bibtex entries, adds new fields (date added, new tags, e.g., "reviews" etc...), and produces a web page with filters and links to PDFs. 
* Donate
[[https://www.paypal.com/donate/?hosted_button_id=Q4DJHCVCRQLJS][static/images/btn_donate_SM.gif]]
