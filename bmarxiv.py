"""
python_arXiv_parsing_example.py

This sample script illustrates a basic arXiv api call
followed by parsing of the results using the 
feedparser python module.

Please see the documentation at 
http://export.arxiv.org/api_help/docs/user-manual.html
for more information, or email the arXiv api 
mailing list at arxiv-api@googlegroups.com.

urllib is included in the standard python library.
feedparser can be downloaded from http://feedparser.org/ .

Author: Julius B. Lucks

This is free software.  Feel free to do what you want
with it, but please play nice with the arXiv API!
"""

# =============================================

try:
    from html import escape
except:
    import html.parser

    escape = html.parser.HTMLParser().escape

import os
from tqdm import tqdm
import requests
import urllib.parse  # encode url
import feedparser
import datetime

try:
    import ruamel.yaml

    yaml = ruamel.yaml.YAML()
    yaml.indent(mapping=4, sequence=4, offset=2)
    yaml.preserve_quotes = True
except:
    import yaml

from copy import deepcopy
import re
import argparse
import pickle
import webbrowser
from os.path import exists

# Opensearch metadata such as totalResults, startIndex,
# and itemsPerPage live in the opensearch namespase.
# Some entry metadata lives in the arXiv namespace.
# This is a hack to expose both of these namespaces in
# feedparser
feedparser.mixin._FeedParserMixin.namespaces[
    "http://a9.com/-/spec/opensearch/1.1/"
] = "opensearch"
feedparser.mixin._FeedParserMixin.namespaces["http://arxiv.org/schemas/atom"] = "arxiv"


# =============================================
def cleanforserialize(entries):
    # these cannot be serialized
    for field in ("summary_fmt", "title_fmt", "authors_fmt"):
        for i, e in enumerate(entries):
            if "keyword_matches" in entries[i]["stats"][field].keys():
                del entries[i]["stats"][field]["keyword_matches"]
                del entries[i]["stats"][field]["keyword_avoid"]
    return entries


# =============================================
# this is to convert dict to clas
class MyObject:
    def __init__(self, d=None):
        if d is not None:
            for key, value in d.items():
                setattr(self, key, value)


# =============================================
# this is for when calling within a wrapper, as function
class args:
    def __init__(
        self,
        configuration_file="",
        sin="",
        last=False,
        display=False,
        test=False,
        # redo=False,
        script=False,
        bib="",
        id="",
        query="",
    ):
        args.configuration_file = str(configuration_file)
        args.sin = str(sin)
        args.last = bool(last)
        args.display = bool(display)
        args.test = bool(test)
        args.bib = str(bib)
        args.id = str(id)
        args.query = str(query)

        # args.redo = bool(redo)


# =============================================
def getlistingday(now_date):
    day = now_date.strftime("%a")
    if day == "Mon":
        date_range = [
            now_date - datetime.timedelta(days=4),
            now_date - datetime.timedelta(days=3),
        ]
    elif day == "Tue":
        date_range = [
            now_date - datetime.timedelta(days=4),
            now_date - datetime.timedelta(days=1),
        ]
    elif day == "Wed":
        date_range = [
            now_date - datetime.timedelta(days=2),
            now_date - datetime.timedelta(days=1),
        ]
    elif day == "Thu":
        date_range = [
            now_date - datetime.timedelta(days=2),
            now_date - datetime.timedelta(days=1),
        ]
    elif day == "Fri":
        date_range = [
            now_date - datetime.timedelta(days=2),
            now_date - datetime.timedelta(days=1),
        ]
    else:
        date_range = None
    if date_range is not None:
        date_range[0] = datetime.datetime.strptime(
            date_range[0].strftime("%d-%m-%Y") + " 16:00 [EST]", date_fmt
        )
        date_range[1] = datetime.datetime.strptime(
            date_range[1].strftime("%d-%m-%Y") + " 15:59 [EST]", date_fmt
        )
    return date_range


# =============================================
def drill_new(entries, base_url, base_query, godrill):
    if godrill[0]:  # first author
        max_results = len(entries) * config["max_results"]["firstauthor"]
        base_query2 = deepcopy(base_query)
        # remove date field
        base_query2["search_query"] = base_query2["search_query"][
            0 : base_query2["search_query"].index(" AND lastUpdatedDate")
        ]
        names = " OR ".join(['"' + e["firstauthor"]["name"] + '"' for e in entries])
        base_query2["search_query"] += f" AND au:({names})"
        details_query = {
            "sortOrder": "descending",
            "sortBy": "lastUpdatedDate",
            "start": 0,
            "max_results": max_results,
        }
        newentries = run(
            base_url, base_query2, details_query, godrill=[False, False], version="new"
        )

        for i, entry in enumerate(entries):
            for newentry in newentries:
                if newentry["firstauthor"].name == entries[i]["firstauthor"].name:
                    entries[i]["score"]["firstauthor"] += newentry["score"]["total"]
                    print(newentry["score"]["total"])
                entries[i]["score"]["firstauthor"] /= config["max_results"][
                    "firstauthor"
                ]

    if godrill[1]:  # other authors
        max_results = len(entries) * config["max_results"]["otherauthors"]
        base_query2 = deepcopy(base_query)
        # remove date field
        base_query2["search_query"] = base_query2["search_query"][
            0 : base_query2["search_query"].index(" AND lastUpdatedDate")
        ]
        names = " OR ".join(
            ['"' + n + '"' for e in entries for n in e["otherauthors"]["name"][0:10]]
        )
        base_query2["search_query"] += f" AND au:{names}"
        details_query = {
            "sortOrder": "descending",
            "sortBy": "lastUpdatedDate",
            "start": 0,
            "max_results": max_results,
        }
        newentries = run(
            base_url, base_query2, details_query, godrill=[False, False], version="new"
        )

        for i, entry in enumerate(entries):
            for newentry in newentries:
                for a in newentry["otherauthors"]:
                    if a.name in [e.name for e in entries[i]["otherauthors"]]:
                        entries[i]["score"]["otherauthors"] += newentry["score"][
                            "total"
                        ]
                        print(newentry["score"]["total"])
                entries[i]["score"]["otherauthors"] /= config["max_results"][
                    "otherauthors"
                ]

    return entries


# =============================================
def drill(entry, base_url, base_query, godrill):
    # first author
    if godrill[0]:
        max_results = config["max_results"]["firstauthor"]
        name = entry["firstauthor"]["name"]
        base_query2 = deepcopy(base_query)
        # remove date field
        if " AND lastUpdatedDate" in base_query2["search_query"]:
            base_query2["search_query"] = base_query2["search_query"][
                0 : base_query2["search_query"].index(" AND lastUpdatedDate")
            ]
            base_query2["search_query"] += f' AND au:"{name}"'
        else:  # args.id provided
            base_query2["search_query"] = f'au:"{name}"'

        details_query = {
            "sortOrder": "descending",
            "sortBy": "lastUpdatedDate",
            "start": 0,
            "max_results": max_results,
        }
        # print(base_url, base_query2, details_query)
        entries = run(base_url, base_query2, details_query, godrill=[False, False])
        # we already considered the most recent one
        entries = entries[1:]
        score = sum([e["score"]["total"] for e in entries])
        # print(score)
        entry["score"].update({"firstauthor": score / max_results})

    if godrill[1]:
        # other authors
        max_results = config["max_results"]["otherauthors"]
        names = " OR ".join(
            [
                '"' + a["name"] + '"'
                for i, a in enumerate(entry["otherauthors"])
                if "name" in a.keys() and i < 10
            ]
        )
        if names != "":
            base_query2 = deepcopy(base_query)
            # remove date field
            base_query2["search_query"] = base_query2["search_query"][
                0 : base_query2["search_query"].index(" AND lastUpdatedDate")
            ]
            base_query2["search_query"] += f" AND au:({names})"
            details_query = {
                "sortOrder": "descending",
                "sortBy": "lastUpdatedDate",
                "start": 0,
                "max_results": max_results,
            }
            entries = run(base_url, base_query2, details_query, godrill=[False, False])
            # we already considered the most recent one
            entries = entries[1:]
            score = sum([e["score"]["total"] for e in entries])
            # print(score)
            entry["score"].update({"otherauthors": score / max_results})

    return entry


# =============================================
def score(entry):
    score = {}
    for field in ("summary_fmt", "title_fmt", "authors_fmt"):
        tmp = len(entry["stats"][field]["keyword_matches"]) - 0.5 * len(
            entry["stats"][field]["keyword_avoid"]
        )

        if field == "summary_fmt":
            norm = len([e for e in entry[field].split() if len(e) > 2])
            if norm > 0:
                tmp = 10 * tmp / norm
        elif field == "title_fmt":
            norm = len([e for e in entry[field].split() if len(e) > 2])
            if norm > 0:
                tmp = 0.5 * tmp / norm
        elif field == "authors_fmt":
            norm = len(entry["authors_fmt"])
            if norm > 0:
                tmp = 0.5 * tmp / norm

        # print(field, tmp, norm)

        tmp *= 100

        score.update({field: tmp})

    score.update(
        {
            "total": score["title_fmt"] + score["authors_fmt"] + score["summary_fmt"],
            "firstauthor": 0,
            "otherauthors": 0,
        }
    )

    allkw, allkwavoid = [], []
    for field in ("summary_fmt", "title_fmt", "authors_fmt"):
        allkw += [
            r.string[r.span(1)[0] : r.span(1)[1]]
            for r in entry["stats"][field]["keyword_matches"]
        ]
        allkwavoid += [
            r.string[r.span(1)[0] : r.span(1)[1]]
            for r in entry["stats"][field]["keyword_matches"]
        ]

    entry["allkw"] = list(set(allkw))
    entry["allkwavoid"] = list(set(allkwavoid))

    entry["score"] = score

    return entry


# =============================================
def run(
    base_url,
    base_query,
    details_query,
    godrill=[True, False],
    verbose=False,
    version="old",
):
    # fullurl = base_url + base_query + details_query
    # print(fullurl)

    # test query
    test_query = {"search_query": "all:electron"}
    d = test_query
    response = requests.post(base_url, data=d)
    if response.status_code != 200:
        print("Query failed")
        exit()
    if len(response.text) < 1000:
        print("Response is not normal, is arXiv API down?")
        print(
            "Check this link in the browser: http://export.arxiv.org/api/query?search_query=all:electron"
        )
        exit()

    d = base_query
    d.update(details_query)
    # print(base_url, d)
    response = requests.post(base_url, data=d)

    if response.status_code != 200:
        print("Query failed")
        exit()

    if verbose:
        print(response.text)

    # parse the response using feedparser
    feed = feedparser.parse(response.text)

    # print feed information
    # print('Feed title: %s' % feed.feed.title)
    # print('Feed last updated: %s' % feed.feed.updated)

    # print(opensearch metadata
    # print('itemsPerPage for this query: %s' % feed.feed.opensearch_itemsperpage)
    # print('startIndex for this query: %s' % feed.feed.opensearch_startindex)
    # print('totalResults for this query: %s' % feed.feed.opensearch_totalresults)

    # =============================================

    for entry in feed.entries:
        entry["type"] = "paper"

    entries = [parse(e) for e in feed.entries]
    if sum(godrill):
        print(f"Number of entries: {len(feed.entries)}")

    # Run through each entry
    tmp = tqdm(entries) if sum(godrill) else entries
    for i, entry in enumerate(tmp):
        entries[i]["type"] = "paper"
        entries[i] = process(entries[i], conf)
        entries[i] = score(entries[i])

        if version == "old":
            if godrill[0] + godrill[1]:
                entries[i] = drill(entries[i], base_url, base_query, godrill)
        # print(entry['score'])

        # print(entry['title'])
        # print(entry['updated'])

    if version == "new":
        entries = drill_new(entries, base_url, base_query, godrill)

    entries = sort(entries)

    return entries


# =============================================
def sort(entries):
    scores_desc = argsort(
        [
            e["score"]["total"] + e["score"]["firstauthor"] + e["score"]["otherauthors"]
            for e in entries
        ]
    )[::-1]
    tmp = []
    for i in range(len(scores_desc)):
        tmp += [entries[scores_desc[i]]]

    return tmp


# =============================================
def highlight(text, res, avoid=False):
    # print(text+"\n")
    hclass = "highlight_alt" if avoid else "highlight"
    if res[0].span()[0] == 0:
        tmp = text[0 : res[0].span()[0]]
    else:
        tmp = text[0 : res[0].span()[0] + 1]

    # print(tmp+"\n")
    for i, r in enumerate(res):
        tmp += f"<font class='{hclass}'>"
        tmp += text[r.span(1)[0] : r.span(1)[1]]  # (1) is for group 1 (captured)
        tmp += "</font>"
        # print(tmp+"\n")
        if i == len(res) - 1:
            tmp += text[r.span(1)[1] :]  # (1) is for group 1 (captured)
        else:
            tmp += text[
                r.span(1)[1] : res[i + 1].span(1)[0]
            ]  # (1) is for group 1 (captured)
        # print(tmp+"\n\n")
    return tmp


# =============================================
def argsort(seq):
    # http://stackoverflow.com/questions/3071415/efficient-method-to-calculate-the-rank-vector-of-a-list-in-python
    return sorted(range(len(seq)), key=seq.__getitem__)


# =============================================
def scan(s, kws):
    matches = []
    kwok = []

    # case-insensitive keywords
    for kw in kws:
        if '"' in kw:
            kw2 = kw.replace('"', "")
            res = [x for x in re.finditer(f"(?:\W+|^)({kw2})(?:\W+|$)", s)]
            # \W to make sure we don't select part of a word (ie select one non-alphanumerical character)
            # ?: for non-capturing groups
            # | or, ^beginning of string, $ end of string
        else:
            res = [
                x for x in re.finditer(f"(?:\W+|^)({kw})(?:\W+|$)", s, re.IGNORECASE)
            ]
            # \W to make sure we don't select part of a word (ie select one non-alphanumerical character)
            # ?: for non-capturing groups
            # | or, ^beginning of string, $ end of string
        if res != []:
            kwok += [
                kw,
            ]
        matches += [r for r in res]

    # remove already considered (e.g., remove galaxy if dwarf galaxy was found)
    final = []
    for i, r in enumerate(matches):
        considered = False
        for j, m in enumerate(matches):
            if i == j:
                continue
            # print(r,m)
            # print(r.span()[0]>=m.span()[0])
            # print(r.span()[0]<=m.span()[1])
            if r.span()[0] >= m.span()[0] and r.span()[1] <= m.span()[1]:
                considered = True
        if not considered:
            final += [
                r,
            ]
    matches = final

    return matches, kwok


# =============================================
def process(entry, conf):
    stats = {}

    for field in ("title_fmt", "summary_fmt", "authors_fmt"):
        matches_avoid = []

        stats.update({field: {}})

        matches, _ = scan(entry[field], conf["keywords"])

        stats[field].update({"keyword_matches": matches})

        if matches != []:
            tmp = argsort([m.span()[0] for i, m in enumerate(matches)])
            matches = [matches[i] for i in tmp]
            entry[field] = highlight(entry[field], matches)

        if field == "authors_fmt":
            # now we can format the author field?
            entry.update(
                {
                    "authors_fmt2": ", ".join(
                        [
                            f"<a href='https://arxiv.org/search/advanced?advanced=&terms-0-operator=AND&terms-0-term=%22{urllib.parse.quote(a)}%22&terms-0-field=author&classification-physics_archives=astro-ph&classification-include_cross_list=include&date-filter_by=all_dates&date-year=&date-from_date=&date-to_date=&date-date_type=submitted_date&abstracts=hide&size=200&order=-announced_date_first' target=_blank class=nodeco>{a}</a>"
                            for a in entry["authors_fmt"].split(", ")
                        ]
                    )
                }
            )

        # unwanted keywords
        if conf["avoid"] is not None:
            for kw in conf["avoid"]:
                if '"' in kw:
                    res = [
                        x
                        for x in re.finditer(f"(?:\W+|^)({kw})(?:\W+|$)", entry[field])
                    ]
                else:
                    res = [
                        x
                        for x in re.finditer(
                            f"(?:\W+|^)({kw})(?:\W+|$)", entry[field], re.IGNORECASE
                        )
                    ]
                matches_avoid += [r for r in res]

            # remove already considered (e.g., remove galaxy if dwarf galaxy was found)
            final = []
            for i, r in enumerate(matches_avoid):
                considered = False
                for j, m in enumerate(matches_avoid):
                    if i == j:
                        continue
                    if r.span()[0] >= m.span()[0] and r.span()[1] <= m.span()[1]:
                        considered = True
                if not considered:
                    final += [
                        r,
                    ]
            matches_avoid = final
        else:
            matches_avoid = []

        stats[field].update({"keyword_avoid": matches_avoid})

        if matches_avoid != []:
            entry[field] = highlight(entry[field], matches_avoid, avoid=True)

    entry.update({"stats": stats})

    return entry


# =============================================
def clean(s):
    s = " ".join(s.split())
    return s.replace("\n", " ")


def parse(feedentry):
    entry = {}

    entry.update({"type": feedentry.type})

    if entry["type"] == "conference":
        entry.update({"link": feedentry.link})
        tmp = [t["name"] for t in feedentry.authors]
        entry.update({"authors": tmp})

        entry.update({"authors_fmt": ", ".join(entry["authors"])})

    entry.update({"title": clean(feedentry.title)})
    entry.update({"title_fmt": entry["title"]})

    if entry["type"] == "code":
        tmp = [t.replace(",", "") for t in feedentry.author]
        entry.update({"authors": tmp})
        entry.update({"authors_fmt": ", ".join(entry["authors"])})
        entry.update({"links_data": feedentry.links_data})
        entry.update({"entry_date": feedentry.entry_date})
        entry.update({"summary": clean(feedentry.abstract)})
    else:
        entry.update({"summary": clean(feedentry.summary)})
    entry.update({"summary_fmt": escape(entry["summary"])})

    if entry["type"] == "paper":
        entry.update({"authors": feedentry.authors})
        entry.update({"authors_fmt": ", ".join([a.name for a in feedentry.authors])})

        entry.update({"firstauthor": feedentry.authors[0]})
        entry.update({"otherauthors": feedentry.authors[1:]})
        entry.update(
            {"primary_category": feedentry["arxiv_primary_category"]["term"]}
        )  # or feedentry.tags[0]['term']
        entry.update({"categories": [t["term"] for t in feedentry.tags]})

        fullid = feedentry.id.split("/abs/")[-1]
        entry.update({"id": fullid.split("v")[0]})
        entry.update({"version": int(fullid.split("v")[1])})
        entry.update({"updated": feedentry.updated})
        entry.update({"link_abs": feedentry.link})
        entry.update({"link_pdf": entry["link_abs"].replace("abs/", "pdf/")})
    else:
        entry.update({"version": 1})
        entry.update({"updated": ""})
        entry.update({"id": ""})
        entry.update({"categories": []})

    if entry["type"] != "code":
        entry.update({"links": feedentry.links})
        entry.update({"published": feedentry.published})

    try:
        tmp = feedentry.arxiv_doi
    except AttributeError:
        tmp = ""
    entry.update({"doi": tmp})

    try:
        tmp = feedentry.arxiv_journal_ref
    except AttributeError:
        tmp = ""
    entry.update({"journal": tmp})

    try:
        tmp = feedentry.arxiv_comment
    except AttributeError:
        tmp = ""
    entry.update({"comment": tmp})

    return entry


# =============================================
def select(entries, template_entry="", template_conf="", template_codes=""):
    newstr = ""
    for entry in entries:
        if entry["type"] == "paper":  # paper
            tmp = deepcopy(template_entry)

            firstauthor = entry["authors_fmt2"].split(",")[0]
            tmp = tmp.replace("{{firstauthor}}", firstauthor)

            otherauthors = entry["authors_fmt2"].split(",")[1:]
            if len(otherauthors) > config["collapseauthors"]:
                otherauthors_short = (
                    ", ".join(
                        entry["authors_fmt2"].split(",")[
                            1 : config["collapseauthors"] - 1
                        ]
                    )
                    + "<button class='toggleSingle' style='border-radius: 2em 2em 2em 2em;border:2px;border-color:black'>et al.</button>"
                )
                otherauthors = ", ".join(
                    entry["authors_fmt2"].split(",")[config["collapseauthors"] - 1 :]
                )
            else:
                otherauthors_short = ", ".join(entry["authors_fmt2"].split(",")[1:])
                otherauthors = ", ".join(entry["authors_fmt2"].split(",")[1:])

            tmp = tmp.replace("{{otherauthors}}", otherauthors)
            tmp = tmp.replace("{{otherauthors_short}}", otherauthors_short)
            tmp = tmp.replace("{{title}}", entry["title_fmt"])
            tmp = tmp.replace("{{comments}}", entry["comment"])
            tmp = tmp.replace("{{summary}}", entry["summary_fmt"])
            totalscore = round(
                entry["score"]["total"]
                + entry["score"]["firstauthor"]
                + entry["score"]["otherauthors"],
                1,
            )
            tmp = tmp.replace(
                "{{totalscore}}",
                str(totalscore),
            )

            tmp = tmp.replace(
                "{{scorebar}}",
                f"""
                <div class="scorebars">
                <hr class="scorebar1">
                <hr class="scorebar1a">
                <hr class="scorebar1b">
                <hr class="scorebar1c">
                <hr class="scorebar2" style="width:{min([100,int(totalscore)])}%">
                </div>""",
            )

            tmp = tmp.replace("{{score}}", str(round(entry["score"]["total"], 1)))
            tmp = tmp.replace(
                "{{firstauthor_score}}", str(round(entry["score"]["firstauthor"], 1))
            )
            tmp = tmp.replace(
                "{{otherauthors_score}}", str(round(entry["score"]["otherauthors"], 1))
            )
            # tmp = tmp.replace('{submitted}', entry['submitted'])
            tmp = tmp.replace("{{published}}", entry["published"])
            tmp = tmp.replace("{{updated}}", entry["updated"])
            tmp = tmp.replace("{{categories}}", ", ".join(entry["categories"]))
            tmp = tmp.replace("{{id}}", entry["id"])
            tmp = tmp.replace("{{url}}", entry["link_abs"])

            if "allkw" in entry.keys():
                tmp = tmp.replace("{{allkw}}", " | ".join(entry["allkw"]))
                tmp = tmp.replace("{{allkwavoid}}", " | ".join(entry["allkwavoid"]))

            newstr += tmp

        elif entry["type"] == "conference":
            tmp = deepcopy(template_conf)

            summary = entry["summary"]
            for s in ("Date", "Location", "Contact", "Address", "Web Site", "E-Mail"):
                summary = summary.replace(s, f"<b><font color=#0055AA>{s}</font></b>")

            tmp = tmp.replace("{{title}}", entry["title"])
            tmp = tmp.replace("{{description}}", summary)
            tmp = tmp.replace("{{link}}", entry["link"])
            tmp = tmp.replace("{{published}}", entry["published"])

            if "score" in entry.keys():
                totalscore = round(
                    entry["score"]["total"],
                    1,
                )
                tmp = tmp.replace("{{totalscore}}", str(totalscore))
                tmp = tmp.replace(
                    "{{scorebar}}",
                    f"""
                    <div class="scorebars">
                    <hr class="scorebar1">
                    <hr class="scorebar1a">
                    <hr class="scorebar1b">
                    <hr class="scorebar1c">
                    <hr class="scorebar2" style="width:{min([100,int(totalscore)])}%">
                    </div>""",
                )
            else:
                tmp = tmp.replace("{{totalscore}}", "")
                tmp = tmp.replace("{{scorebar}}", "")

            if "allkw" in entry.keys():
                tmp = tmp.replace("{{allkw}}", " | ".join(entry["allkw"]))
                tmp = tmp.replace("{{allkwavoid}}", " | ".join(entry["allkwavoid"]))

            newstr += tmp

        elif entry["type"] == "code":
            tmp = deepcopy(template_codes)

            tmp = tmp.replace("{{title}}", entry["title_fmt"])

            tmp = tmp.replace("{{summary}}", entry["summary_fmt"])

            tmp = tmp.replace("{{authors}}", entry["authors_fmt"])

            tmp = tmp.replace("{{link}}", eval(entry["links_data"][0])["url"])

            dd = datetime.datetime.strptime(entry["entry_date"], "%Y-%m-%dT%H:%M:%SZ")
            dd = dd.strftime("%d-%m-%Y")

            tmp = tmp.replace("{{published}}", dd)

            if "score" in entry.keys():
                totalscore = round(
                    entry["score"]["total"],
                    1,
                )
                tmp = tmp.replace("{{totalscore}}", str(totalscore))
                tmp = tmp.replace(
                    "{{scorebar}}",
                    f"""
                    <div class="scorebars">
                    <hr class="scorebar1">
                    <hr class="scorebar1a">
                    <hr class="scorebar1b">
                    <hr class="scorebar1c">
                    <hr class="scorebar2" style="width:{min([100,int(totalscore)])}%">
                    </div>""",
                )
            else:
                tmp = tmp.replace("{{totalscore}}", "")
                tmp = tmp.replace("{{scorebar}}", "")

            if "allkw" in entry.keys():
                tmp = tmp.replace("{{allkw}}", " | ".join(entry["allkw"]))
                tmp = tmp.replace("{{allkwavoid}}", " | ".join(entry["allkwavoid"]))

            newstr += tmp

    return newstr


# =============================================
def makepage_others(config, conferences=None, codes=None):
    with open("templates/template_others.html", "r") as f:
        template_o = f.readlines()
    template_o = "\n".join(template_o)

    # ==================

    if conferences is not None and conferences != []:
        with open("templates/template_conf.html", "r") as f:
            template_conf = f.readlines()
        template_conf = "\n".join(template_conf)
        newstr = select(conferences, template_conf=template_conf)
        template_o = template_o.replace("{{conferences}}", newstr)
        template_o = template_o.replace("{{conferences_display}}", "contents")
    else:
        template_o = template_o.replace("{{conferences_display}}", "none")

    conf = config["queries"]

    # ==================

    if codes is not None and codes != []:
        with open("templates/template_codes.html", "r") as f:
            template_codes = f.readlines()
        template_codes = "\n".join(template_codes)
        newstr = select(codes, template_codes=template_codes)
        template_o = template_o.replace("{{codes}}", newstr)
        template_o = template_o.replace("{{codes_display}}", "contents")
    else:
        template_o = template_o.replace("{{codes_display}}", "none")

    conf = config["queries"]

    with open(f"results/bmarxiv_others.html", "w") as f:
        f.writelines(template_o)

    return template_o


# =============================================
def makepage(entries, name, config):
    with open("templates/template_papers.html", "r") as f:
        template = f.readlines()
    template = "\n".join(template)

    template = template.replace("{{entries_total}}", str(len(entries)))

    conf = config["queries"][name]

    template = template.replace(
        "{{categories}}", ", ".join(config["categories"]["included"])
    )
    template = template.replace(
        "{{excategories}}", ", ".join(config["categories"]["excluded"])
    )
    # ==================

    with open("templates/template_entry.html", "r") as f:
        template_entry = f.readlines()
    template_entry = "\n".join(template_entry)

    with open("templates/template_conf.html", "r") as f:
        template_conf = f.readlines()
    template_conf = "\n".join(template_conf)

    with open("templates/template_codes.html", "r") as f:
        template_codes = f.readlines()
    template_codes = "\n".join(template_codes)

    entries_tmp = [e for e in entries if e["score"]["total"] > 0 and e["version"] == 1]
    newstr = select(
        entries_tmp,
        template_entry=template_entry,
        template_conf=template_conf,
        template_codes=template_codes,
    )
    template = template.replace("{{display_entries}}", newstr)
    template = template.replace("{{entries_match_new}}", str(len(entries_tmp)))

    entries_tmp = [
        e
        for e in entries
        if e["score"]["total"] == 0
        and (e["score"]["firstauthor"] + e["score"]["otherauthors"]) > 0
        and e["version"] == 1
    ]
    newstr = select(
        entries_tmp,
        template_entry=template_entry,
        template_conf=template_conf,
        template_codes=template_codes,
    )
    template = template.replace("{{display_entries_author}}", newstr)

    entries_tmp = [e for e in entries if e["score"]["total"] > 0 and e["version"] > 1]
    newstr = select(
        entries_tmp,
        template_entry=template_entry,
        template_conf=template_conf,
        template_codes=template_codes,
    )
    template = template.replace("{{display_entries_updates}}", newstr)
    template = template.replace("{{entries_match_revised}}", str(len(entries_tmp)))

    entries_tmp = [
        e
        for e in entries
        if e["score"]["total"] == 0
        and (e["score"]["firstauthor"] + e["score"]["otherauthors"]) == 0
        and e["version"] == 1
    ]
    newstr = select(
        entries_tmp,
        template_entry=template_entry,
        template_conf=template_conf,
        template_codes=template_codes,
    )
    template = template.replace("{{display_entries_zero}}", newstr)

    entries_tmp = [
        e
        for e in entries
        if e["score"]["total"] == 0
        and (e["score"]["firstauthor"] + e["score"]["otherauthors"]) == 0
        and e["version"] > 1
    ]
    newstr = select(
        entries_tmp,
        template_entry=template_entry,
        template_conf=template_conf,
        template_codes=template_codes,
    )
    template = template.replace("{{display_entries_zero_updates}}", newstr)

    template = template.replace("{{keywords}}", " / ".join(conf["keywords"]))
    template = template.replace("{{name}}", name)
    # template = template.replace('{categories}', ' / '.join(categories))

    with open(f"results/bmarxiv_{name}.html", "w") as f:
        f.writelines(template)

    # print(template)
    return template


# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
# =============================================
def main(args=None):
    if args is None:
        parser = argparse.ArgumentParser(description="bmarxiv")
        parser.add_argument(
            "--configuration-file",
            "-c",
            type=str,
            default="config.yaml",
            help="Configuration file",
        )
        parser.add_argument(
            "--streamin",
            "-sin",
            type=str,
            help="Use string for yaml input [script mode]",
        )
        parser.add_argument(
            "--display",
            "-d",
            action="store_true",
            help="Just display results (no update)",
        )
        parser.add_argument(
            "--last", "-l", action="store_true", help="Check latest listing"
        )
        parser.add_argument(
            "--test", "-t", action="store_true", help="Test a standard query"
        )
        parser.add_argument("--id", "-i", type=str, help="Test a specific arxiv ID")
        parser.add_argument(
            "--bib",
            "-b",
            type=str,
            help="Read .bib file to scan for potential keywords (provide also query with -q)",
        )
        parser.add_argument("--query", "-q", type=str, help="Use query for bib option")
        # parser.add_argument("--redo",
        #                     "-r",
        #                     action='store_true',
        #                     help="Redo query (using previous last checked date)")
        args = parser.parse_args()

    # =============================================
    global config, date_fmt

    os.makedirs("results/", exist_ok=True)

    date_fmt = "%d-%m-%Y %H:%M [EST]"

    if args.streamin is not None:
        config = yaml.load(args.streamin)
        print(config)
    else:
        try:
            with open(args.configuration_file) as file:
                try:
                    config = yaml.load(file)
                    print(config)
                except yaml.YAMLError as exc:
                    print(exc)
        except:
            print(f"Configuration file {args.configuration_file} not found")
            print(
                "Create your own configuration file by copying the example file into config.yaml or else use an existing file with -c"
            )
            exit()

    # =============================================

    wb = webbrowser
    if "browser" in config.keys():
        if "command" in config["browser"].keys():
            wb = webbrowser.get(config["browser"]["command"])

    # =============================================

    if args.bib:
        if not args.query:
            print("Provide query name with -q")
            exit()

        from scanbib import scanbib

        res = scanbib(args.bib, config["queries"][args.query]["keywords"])

        nomatch_str = ""
        if len(res["nomatch"]) > 1:
            nomatch_str = f"""
            Found keywords with no match (you may wanna modify/remove them): 
            <br>
            <font color=red>{res['nomatch']}</font>
            <br>"""

        res2 = ["", "", ""]

        for i in range(3):
            for r in res[i + 1]:
                tmp, kwok = scan(r, config["queries"][args.query]["keywords"])
                considered = ", ".join(kwok) if tmp != [] else " "
                c = "white" if tmp != [] else "#FAA"
                res2[
                    i
                ] += f"<tr><td bgcolor={c}>{r}</td><td>{res[i+1][r]}</td><td>{considered}</td></tr>"

        s = f"""<html><title>Bibliography scan for query: {args.query}</title><body>

        <h2>The following lists are based on a random sample from the bibliography file provided, it is not complete</h2

        {nomatch_str}

        <table border=0 cellpadding=10>
        <tr>

        <td valign=top>        
        <table border=1 style='border: solid 1px'>
        <tr><td>3 words</td><td></td><td>Match</td></tr>
        {res2[2]}
        </table>
        </td>

        <td valign=top>
        <table border=1 style='border: solid 1px'>
        <tr><td>2 words</td><td></td><td>Match</td></tr>
        {res2[1]}
        </table>
        </td>

        <td valign=top>        
        <table border=1 style='border: solid 1px'>
        <tr><td>1 word</td><td></td><td>Match</td></tr>
        {res2[0]}
        </table>
        </td>

        </tr>
        </table>
        </body>
        </html>"""

        with open(f"results/scanbib_{args.query}.html", "w") as f:
            f.writelines(s)

        wb.open(
            f"results/scanbib_{args.query}.html",
            new=0,
            autoraise=True,
        )

        return

    if args.display:
        try:
            with open(f"results/bmarxiv_conferences.pkl", "rb") as f:
                conferences = pickle.load(f)
        except:
            conferences = None

        try:
            with open(f"results/bmarxiv_codes.pkl", "rb") as f:
                codes = pickle.load(f)
        except:
            codes = None

        conferences = [parse(e) for e in conferences]
        codes = [parse(e) for e in codes]
        makepage_others(config, conferences=conferences, codes=codes)
        if (
            conferences is not None
            and codes is not None
            and conferences != []
            and codes != []
        ):
            wb.open(
                f"results/bmarxiv_others.html",
                new=0,
                autoraise=True,
            )

        for queryname in config["queries"]:
            if args.query is not None:
                if queryname != args.query:
                    continue

            if "disabled" in config["queries"][queryname]:
                if bool(config["queries"][queryname]["disabled"]):
                    continue

            with open(f"results/bmarxiv_{queryname}.pkl", "rb") as f:
                entries = pickle.load(f)

                scores = [
                    e["score"]["total"]
                    + e["score"]["firstauthor"]
                    + e["score"]["otherauthors"]
                    for e in entries
                ]

                entries = sort(entries)
                # [
                #     entries[argsort(scores)[::-1][i]] for i in range(len(scores))
                # ]

                print(f"\nMaking page for {queryname}")
                makepage(entries, queryname, config)

            wb.open(f"results/bmarxiv_{queryname}.html", new=0, autoraise=True)

        return

    # =============================================

    max_results = 2000
    if "max_results" in config:
        if config["max_results"] is not None:
            max_results = config["max_results"]["listing"]

    # =============================================

    now_date = datetime.datetime.today() + datetime.timedelta(hours=config["eastcoast"])
    now = now_date.strftime(date_fmt)

    # =============================================

    # will contain results for script use
    results = {}

    # =============================================
    if config["codes"] is not None:
        print("\nChecking codes")

        success = False
        if "ads" not in config.keys():
            print(
                "Please provide token or token file location under ads in configuration file"
            )
        elif "token" not in config["ads"].keys():
            print(
                "Please provide token or token file location under ads in configuration file"
            )
        else:
            if config["ads"]["token"] is not None:
                if exists(config["ads"]["token"]):
                    with open(config["ads"]["token"]) as f:
                        adstoken = f.readlines()
                        adstoken = adstoken[0].replace("\n", "").strip()
                else:
                    adstoken = config["ads"]["token"]
                try:
                    r = requests.get(
                        "https://api.adsabs.harvard.edu/v1/search/query",
                        params={
                            "q": 'bibstem:"ascl.soft"',
                            "start": "0",
                            "rows": "100",
                            "sort": "date desc",
                            "wt": "json",
                            "fl": [
                                "title",
                                "date",
                                "entry_date",
                                "author",
                                "links_data",
                                "abstract",
                            ]
                            # Allowed: abstract ┃ ack ┃ aff ┃ aff_id ┃ alternate_bibcode ┃ alternate_title ┃ arxiv_class ┃ author ┃ author_count ┃ author_norm ┃ bibcode ┃ bibgroup ┃ bibstem ┃ citation ┃ citation_count ┃ cite_read_boost ┃ classic_factor ┃ comment ┃ copyright ┃ data ┃ database ┃ date ┃ doctype ┃ doi ┃ eid ┃ entdate ┃ entry_date ┃ esources ┃ facility ┃ first_author ┃ first_author_norm ┃ grant ┃ grant_agencies ┃ grant_id ┃ id ┃ identifier ┃ indexstamp ┃ inst ┃ isbn ┃ issn ┃ issue ┃ keyword ┃ keyword_norm ┃ keyword_schema ┃ lang ┃ links_data ┃ nedid ┃ nedtype ┃ orcid_pub ┃ orcid_other ┃ orcid_user ┃ page ┃ page_count ┃ page_range ┃ property ┃ pub ┃ pub_raw ┃ pubdate ┃ pubnote ┃ read_count ┃ reference ┃ simbid ┃ title ┃ vizier ┃ volume ┃ year
                        },
                        headers={"Authorization": f"Bearer:{adstoken}"},
                    )
                    success = True
                except:
                    print("Request failed...")

                tmp = eval(r.content.decode())
                if "response" not in tmp.keys():
                    success = False
                    print("Request ok but no response, check ADS token")

        if success:
            code_entries = tmp["response"]["docs"]

            codes_lastdigest = datetime.datetime.strptime(
                config["codes"]["lastdigest"], date_fmt
            )

            # find out most recent digest
            mostrecent = datetime.datetime.strptime("11-08-1900 00:00 [EST]", date_fmt)
            for entry in code_entries:
                pubdate = datetime.datetime.strptime(
                    entry["entry_date"], "%Y-%m-%dT%H:%M:%SZ"
                )
                if pubdate > mostrecent:
                    mostrecent = pubdate

            entries = []
            for entry in code_entries:
                pubdate = datetime.datetime.strptime(
                    entry["entry_date"], "%Y-%m-%dT%H:%M:%SZ"
                )
                # print(pubdate, codes_lastdigest)
                if pubdate > codes_lastdigest:
                    entry["type"] = "code"
                    entry = MyObject(entry)
                    entry.title = entry.title[0]
                    entries += [
                        entry,
                    ]
                    print("New code found: {}".format(entry.title))
            with open(f"results/bmarxiv_codes.pkl", "wb") as f:
                pickle.dump(entries, f)

            config["codes"]["lastlastdigest"] = deepcopy(config["codes"]["lastdigest"])
            config["codes"]["lastdigest"] = mostrecent.strftime(date_fmt)

            results.update({"codes": entries})
            codes = entries

    # =============================================
    if config["conferences"] is not None:
        print("\nChecking conferences")
        print(config["conferences"]["url"])

        status = True
        try:
            response = requests.get(config["conferences"]["url"])
            if response.status_code != 200:
                print("Query failed [conferences]")
                status = False
        except:
            print("Problem with request")
            status = False

        if status:
            if config["conferences"]["lastchecked"] is None:
                print(f"\nChecking last 24h")
                conf_lastchecked = now_date - datetime.timedelta(days=1)
            else:
                conf_lastchecked = datetime.datetime.strptime(
                    config["conferences"]["lastchecked"], date_fmt
                )
            print("From: ", conf_lastchecked)
            feed = feedparser.parse(response.text)
            entries = []
            for entry in feed.entries:
                pubdate = datetime.datetime.strptime(
                    entry["published"], "%a, %d %b %Y %H:%M:%S GMT"
                )
                # print(pubdate, conf_lastchecked)
                if pubdate > conf_lastchecked:
                    entry["type"] = "conference"
                    entry = MyObject(entry)
                    entries += [
                        entry,
                    ]
                    print("New conference found: {}".format(entry.title))
            with open(f"results/bmarxiv_conferences.pkl", "wb") as f:
                pickle.dump(entries, f)

            config["conferences"]["lastlastchecked"] = deepcopy(
                config["conferences"]["lastchecked"]
            )
            config["conferences"]["lastchecked"] = now

            results.update({"conferences": entries})
            conferences = entries

    # =============================================
    # defaults

    try:
        overallcategories = config["categories"]["included"]
    except:
        raise Exception("Need some categories")

    try:
        overallexcategories = config["categories"]["excluded"]
    except:
        overallexcategories = None

    try:
        overallscoresfirstauthor = bool(config["scores"]["firstauthor"])
    except:
        overallscoresfirstauthor = False

    try:
        overallscoresotherauthors = bool(config["scores"]["otherauthors"])
    except:
        overallscoresotherauthors = False

    # =============================================

    # Base api query url
    base_url = "https://export.arxiv.org/api/query?search_query"

    if args.test:
        base_query = {"search_query": "all:electron"}
        details_query = {"start": 0, "max_results": 10}
        entries = run(
            base_url,
            base_query,
            details_query,
            godrill=False,
            verbose=True,
            version="new",
        )
        return

    for iq, queryname in enumerate(config["queries"]):
        if args.query is not None:
            if queryname != args.query:
                continue

        print(f"\nChecking listings for {queryname}")

        global conf
        conf = config["queries"][queryname]

        if "disabled" in conf:
            if bool(conf["disabled"]):
                continue

        if conf["lastchecked"] is None:
            args.last = True
        else:
            lastchecked = conf["lastchecked"]

        # override with previous date if need to redo
        # if args.redo:
        #     lastchecked = conf['lastlastchecked']

        # =========================================
        # override with local categories if needed
        categories = deepcopy(overallcategories)
        excategories = deepcopy(overallexcategories)
        if "categories" in conf:
            if conf["categories"] is not None:
                categories = conf["categories"]["included"]
                excategories = conf["categories"]["excluded"]

        # make strings
        categories_str = "cat:(" + " OR cat:".join(categories) + ")"
        if excategories is None or excategories == [None]:
            excategories_str = ""
        else:
            excategories_str = " AND NOT cat:(" + " OR ".join(excategories) + ")"

        # =========================================
        # override with scores if needed
        scorefirstauthor = deepcopy(overallscoresfirstauthor)
        scoreotherauthors = deepcopy(overallscoresotherauthors)
        if "scores" in conf:
            if conf["scores"] is not None:
                scorefirstauthor = conf["scores"]["firstauthor"]
                scoreotherauthors = conf["scores"]["otherauthors"]

        # =========================================

        # keywords = conf["keywords"]
        # keywords_str = " OR ".join(keywords)
        # "notinterested" keywords will be processed afterwards

        # =========================================

        if args.id is None:
            if args.last:
                date_range = getlistingday(now_date)
                if date_range is None:
                    print("No listing (week-end?)")
                    continue
            else:
                from_date = datetime.datetime.strptime(lastchecked, date_fmt)
                date_range = getlistingday(from_date)
                print(from_date, date_range)
                if date_range is None:
                    while from_date <= now_date and date_range is None:
                        from_date += datetime.timedelta(days=1)
                        date_range = getlistingday(from_date)
                    if date_range is None:
                        print("Starting date has no listing")
                        continue

                date_range[0] = date_range[1]
                # go through all listings from the last checked
                while from_date <= now_date:
                    tmp = getlistingday(from_date)
                    if tmp is not None:
                        date_range[1] = tmp[1]
                    from_date += datetime.timedelta(hours=1)

            from_query = date_range[0].strftime("%Y%m%d%H%M00")
            to_query = date_range[1].strftime("%Y%m%d%H%M59")
            print("From: ", date_range[0].strftime(date_fmt))
            print("To: ", date_range[1].strftime(date_fmt))
            conf["lastlastchecked"] = deepcopy(conf["lastchecked"])
            conf["lastchecked"] = now  # date_range[1].strftime(date_fmt)

        if args.id is not None:
            base_query = {"search_query": f"id:{args.id}"}
        else:
            base_query = {
                "search_query": f"{categories_str}{excategories_str} AND lastUpdatedDate:[{from_query} TO {to_query}]"
            }
            # base_query = {'search_query': f'{categories_str}{excategories_str} AND au:x AND lastUpdatedDate:[{from_query} TO {to_query}]'} #test
        details_query = {
            "sortOrder": "descending",
            "sortBy": "lastUpdatedDate",
            "start": 0,
            "max_results": max_results,
        }

        print(base_query)

        entries = run(
            base_url,
            base_query,
            details_query,
            godrill=[
                scorefirstauthor,
                scoreotherauthors,
            ],
            # old version by default
        )
        try:
            with open(f"results/bmarxiv_conferences.pkl", "rb") as f:
                conferences = pickle.load(f)
        except:
            conferences = None
        confentries = []
        if conferences is not None and conferences != []:
            confentries = [parse(e) for e in conferences]
            # Run through each entry
            for i, entry in enumerate(confentries):
                confentries[i] = process(confentries[i], conf)
                confentries[i] = score(confentries[i])
                if confentries[i]["score"]["total"] > 0:
                    entries += [
                        confentries[i],
                    ]

        try:
            with open(f"results/bmarxiv_codes.pkl", "rb") as f:
                codes = pickle.load(f)
        except:
            codes = None
        codeentries = []
        if codes is not None and codes != []:
            codeentries = [parse(e) for e in codes]
            # Run through each entry
            for i, entry in enumerate(codeentries):
                codeentries[i] = process(codeentries[i], conf)
                codeentries[i] = score(codeentries[i])
                if codeentries[i]["score"]["total"] > 0:
                    entries += [
                        codeentries[i],
                    ]

        entries = sort(entries)

        results.update({queryname: entries})

        if args.streamin is not None:
            print("Not updating [script]...")
            return (
                makepage(entries, queryname, config),
                makepage_others(config, conferences=conferences, codes=codes),
            )
        else:
            if len(entries) > 0:
                print(f"Updating {queryname}...")

                entries = cleanforserialize(entries)
                with open(f"results/bmarxiv_{queryname}.pkl", "wb") as f:
                    pickle.dump(entries, f)

                makepage(entries, queryname, config)
                wb.open(f"results/bmarxiv_{queryname}.html", new=0, autoraise=True)

                # update if everything went well
                with open(args.configuration_file, "w") as file:
                    yaml.dump(config, file)
            else:
                print(f"No resuts, not updating {queryname}...")

    if not args.id:
        if (conferences is not None and conferences != []) or (
            codes is not None and codes != []
        ):
            makepage_others(config, conferences=confentries, codes=codeentries)
            wb.open(
                f"results/bmarxiv_others.html",
                new=0,
                autoraise=True,
            )

        if args.streamin is None:
            with open(args.configuration_file, "w") as file:
                yaml.dump(config, file)

    return results


# --------------------------------------------------------------------
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupt!")
