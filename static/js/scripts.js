<!--

//======================================================================
//Toggle score null

// loading message
    $('#loading').hide();
// score = 0 div
jQuery(document).ready(function(){
    jQuery('#togglescorenull').on('click', function(event) {        
        jQuery('#scorenull').toggle('show');
        if ($.trim($(this).text()) === 'Show') { $(this).text('Hide'); } else { $(this).text('Show'); } 
    });
});	


//======================================================================
//Toggle menu

// loading message
    $('#loading').hide();
// score = 0 div
jQuery(document).ready(function(){
    jQuery('#togglemenu').on('click', function(event) {
        jQuery('#menu').toggle('show');
        if ($.trim($(this).text()) === 'Show sections') { $(this).text('Hide sections'); } else { $(this).text('Show sections'); } 
    });
});	



//======================================================================
//Toggle Mathjax

var isTypeset = true;
function ToggleMJ(button) {
  button.disabled = true;
  button.value = (isTypeset ? "Enable Mathjax" : "Disable Mathjax");
  MathJax.Hub.Queue(
    (isTypeset ? RemoveMJ : ["Typeset",MathJax.Hub]),
    function () {
      isTypeset = !isTypeset;
      button.disabled = false;
    }
  );
}

function RemoveMJ() {
  var jax = MathJax.Hub.getAllJax();
  for (var i = 0, m = jax.length; i < m; i++) {
    var tex = jax[i].originalText;
    var isDisplay = (jax[i].root.Get("display") === "block");
    if (isDisplay) tex = "$$"+tex+"$$"; else tex = "$"+tex+"$";
    var script = jax[i].SourceElement();
    jax[i].Remove();
    var preview = script.previousSibling;
    if (preview && preview.className === "MathJax_Preview") 
      preview.parentNode.removeChild(preview);
    script.parentNode.insertBefore(document.createTextNode(tex),script);
    script.parentNode.removeChild(script);
  }
}

-->
